# Role to deploy matrix-synapse

This role will deploy synapse with a postgres db via the pip
aproach. No virtualenv is used. Instead a dedicated service user is
created and synapse-matrix is installed for that user.

By default the deployment uses the same config path, service username
as an installation from a prebuild debian dpkg.

Use it in a playbook like this:

```yml
- hosts: <host>
  roles:
    - role: matrix
      vars:
        matrix_domain: matrix.<domain>
        matrix_ssl_cert: /etc/letsencrypt/live/matrix.<domain>/fullchain.pem
        matrix_ssl_key: /etc/letsencrypt/live/matrix.<domain>/privkey.pem
        matrix_db_pass: "<pass>"
```

If you pass `matrix_sql_dump_repo`, `matrix_sql_dump` and `git_deploy_key`, then the
role will check out a git repo from `matrix_sql_dump_repo` using the
deploy key. It will assume,
that the repo contains a sql file `matrix_sql_dump`, which will be restored into the db.
Also this will create a cron job which will dump the db to that file
and push it to the repo.
