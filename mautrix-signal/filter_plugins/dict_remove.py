class FilterModule:
    @staticmethod
    def filters():
        return {"dict_remove": dict_remove}


def dict_remove(dct, keys):
    for key in keys:
        *path, leaf = key.split(".")
        current = dct
        for component in path:
            current = current[component]
        current.pop(leaf)
    return dct
