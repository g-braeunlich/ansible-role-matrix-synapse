# mautrix-signal

Installs
[mautrix-signal](https://docs.mau.fi/bridges/python/setup/systemd.html?bridge=signal)
in relay mode (this means instead of a 1:1 mapping between signal and
matrix users, all matrix messages are sent via only one signal account
and are prefixed with the sender's nick name).

## Manual steps needed after setup

* [Linking to a device or register a phone number](https://docs.mau.fi/bridges/python/signal/authentication.html)
* In a signal room, invite the bridge number. This should
  automatically create a matrix room in the homeserver the
  bridge lives in and invite its admin.
* Enable relay mode for the room: Use `!signal set-relay` in the matrix room
